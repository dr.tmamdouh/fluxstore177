import 'package:flutter/material.dart';
import 'package:pedantic/pedantic.dart';

import '../../common/constants.dart';
import '../../services/index.dart';
import 'category.dart';

class CategoryModel with ChangeNotifier {
  final Services _service = Services();
  List<Category> categories;
  Map<String, Category> categoryList = {};

  bool isLoading = false;
  String message;

  Future<void> getCategories({lang, cats}) async {
    try {
      printLog("[Category] getCategories");
      isLoading = true;
      notifyListeners();
      categories = await _service.getCategories(lang: lang);

      message = null;
      if (cats != null) {
        List<Category> _categories = [];
        List<Category> _subCategories = [];
        bool isParent = true;
        for (var cat in cats) {
          Category item = categories.firstWhere(
              (element) => element.id.toString() == cat,
              orElse: () => null);
          if (item != null) {
            if (item.parent != '0') {
              isParent = false;
            }
            _categories.add(item);
          }
        }
        for (var cat in categories) {
          Category item = _categories.firstWhere(
              (element) => element.id == cat.id,
              orElse: () => null);
          if (item == null && isParent && cat.parent != '0') {
            _subCategories.add(cat);
          }
        }
        categories = [..._categories, ..._subCategories];
      }
      for (Category cat in categories) {
        categoryList[cat.id] = cat;
      }
      isLoading = false;
      notifyListeners();

      /// use for second category screens so that we don't need to await here
      if (kIsWeb) {
        unawaited(_service.getCategoryWithCache());
      }
    } catch (err, _) {
      isLoading = false;
      message = "There is an issue with the app during request the data, "
              "please contact admin for fixing the issues " +
          err.toString();
      notifyListeners();
    }
  }
}
