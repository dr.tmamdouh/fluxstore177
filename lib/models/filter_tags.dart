import 'package:flutter/material.dart';

import '../services/index.dart';

class FilterTagModel with ChangeNotifier {
  List<FilterTag> lstProductTag;
  final Services _service = Services();
  bool isLoading = false;

  Future<void> getFilterTags() async {
    try {
      isLoading = true;
      notifyListeners();
      lstProductTag = await _service.getFilterTags();
      if (lstProductTag == null || lstProductTag.isEmpty) {
        isLoading = false;
        notifyListeners();
        return;
      }
      // Remove duplicates item
      for (var index = 0; index < lstProductTag.length; index++) {
        final currentProduct = lstProductTag[index];
        final listDuplicate = lstProductTag
            .where((element) => currentProduct.id == element.id)
            .toList();
        if (listDuplicate.length > 1) {
          for (var indexDup = 1; indexDup < listDuplicate.length; indexDup++) {
            lstProductTag.remove(listDuplicate[indexDup]);
          }
        }
      }
      isLoading = false;
      notifyListeners();
    } catch (_) {}
  }
}

class FilterTag {
  int id;
  String slug;
  String name;
  String description;
  int count;

  FilterTag({this.id, this.name, this.slug, this.description, this.count});

  FilterTag.fromJson(Map<String, dynamic> parsedJson) {
    id = parsedJson['id'];
    slug = parsedJson['slug'];
    name = parsedJson['name'];
    description = parsedJson['description'];
    count = parsedJson['count'];
  }
}
